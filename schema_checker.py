import re
from typing import Callable

import util.error_messages as messages

errors: list = []

def __check(obj: dict, schema: dict, currentKey=None):
    '''
    Validates whether the provided data structure is compatible with the schema.

    Args:
        data_structure (dict): The data structure to validate.
        validation_schema (dict): The schema to compare against.
        current_key (None | str): The current key being checked.
    Returns:
        None: 
    '''
    if not isinstance(obj, dict):
        if str(type(obj).__name__) != schema['type']:
            errors.append({
                'element': currentKey, 'message': messages.TYPE
            })
        if isinstance(obj, (str, list)) and schema.get('maxLength'):
            if len(obj) > schema['maxLength']:
                errors.append({
                    'element': currentKey, 'message': messages.MAX_LENGTH
                })
        if schema.get('pattern'):
            pattern  = re.compile(str( schema['pattern']))
            re.fullmatch(pattern, obj) is None and errors.append({
                 'element': currentKey, 'message': messages.PATTERN
            })

        return

    schemaKeys = list(schema.keys())

    for i, key in enumerate(schemaKeys):
        if (obj.get(key) is None) and (schema[key].get('required') is True):
            errors.append(
                {'element': schemaKeys[i], 'message': messages.REQUIRED})
        elif obj.get(schemaKeys[i]) is None:
            errors.append(
                {'element': schemaKeys[i], 'message': messages.NOT_FOUND}
            )
        else:
            __check(obj[key], schema[key], schemaKeys[i])

def validate (obj: dict, schema: dict, callback: Callable):
    try:
        __check(obj, schema, None)
        callback(errors)
        errors.clear()
    except Exception as e:
        print('Something went wrong...', e.args)