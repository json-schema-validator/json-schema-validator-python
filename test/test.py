import json

from schema_checker import validate
import util.error_messages as messages

def callback (result: list = []):
    global errorList
    errorList = result.copy()

def userFail(schema: dict):
    data =  {
        'address': {
            'state': 'Nevada',
            'city': {
                'neighborhood': 'Green Valley North',
                'street': 'E Proctor St',
                'number': 30
            }
        },
        'age': 28,
        'email': 'rogs@yahoo.com'
    }

    print('Running tests for test_age_fail...\nResult:')
    validate(data, schema, callback)

    if( {'element': 'name', 'message': messages.REQUIRED} in errorList):
        print('\tSuccess ✅')
        return
    print ('\tError ❌', errorList)

def userSuccess(schema: dict):
    data =  {
        'address': {
            'state': 'Nevada',
            'city': {
                'neighborhood': 'Green Valley North',
                'street': 'E Proctor St',
                'number': 30
            }
        },
        'age': 28,
        'email': 'rogs_skf@yahoo.com',
        'name': 'Rogério'
    }

    print('Running tests for user_success...\nResult:')
    validate(data, schema, callback)

    if len(errorList) == 0:
        print('\tSuccess ✅')
        return
    print ('\tError ❌', errorList)

def productDescriptionSuccess( schema: dict):
    data = {
        'productDescription': 'Chopped tomatoes',
        'value': 1.95,
        'date': '2023-06-12'
    }
    print('Running tests for product_success...\nResult:')
    validate(data, schema, callback)
    if len(errorList) == 0:
        print('\tSuccess ✅')
        return
    print ('\tError ❌', errorList)

def authSuccess(schema: dict):
    data = {
        'auth': {
            'auth_time': 1311280969,
            'exp': 1311281970,
            'iat': 1311280970,
            'sub': 'Alice',
            'profile': {
                'email': 'user@outlook.com',
                'roles': ['producer', 'consumer', 'attendant']
            }
        }
    }
    print('Running tests for auth_success...\nResult:')
    validate(data, schema, callback)
    if len(errorList) == 0:
        print('\tSuccess ✅')
        return
    print ('\tError ❌', errorList)


def start ():
    """Runs the test suite.

    Tests are composed of assertions about success and failure
    involving some custom JSON structs.
    """
    userSchemaFile = open('schemas/user_schema.json')
    billSchemaFile = open('schemas/bill_schema.json')
    authSchemaFile = open('schemas/auth_schema.json')

    userSchema = json.load(userSchemaFile)
    billSchema = json.load(billSchemaFile)
    authSchema = json.load(authSchemaFile)

    # Product test set
    productDescriptionSuccess(billSchema)
    # Address test set
    userSuccess(userSchema)
    userFail(userSchema)
    # Auth test set
    authSuccess(authSchema)
    