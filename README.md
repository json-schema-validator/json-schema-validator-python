# JSON Schema Validator

A lightweight and straightforward algorithm that validates a JSON document against a given schema. The code is built without any dependencies and can be easily replicated in any Python-based application.

Despite the code shown here being perfectly applicable in "real-life apps," its main goal is to teach developers that it is not only possible but also feasible to build a validator like this, without the need for third-party dependencies.

JSON validation is extremely useful for a variety of scenarios, including communication between processes, APIs, printers, and applications. This format is widely used for data transportation, and depending on its purpose, it is necessary to establish "contracts" regarding the data format. Such a principle is important to ensure that the recipient receives the relevant content.

Finally, it is important to highlight that this code is merely illustrative. Feel free to make adjustments as per your requirements.

## How does it work?

The easiest way to iterate through the entire JSON document is to start at the top (root) and traverse down through all the child nodes by passing each reached node (root) into a recursive method. The check() method utilizes this technique to traverse through nodes.

For each node in the schema, we check if the current object subtree contains the corresponding key at the same level.

The algorithm in this project uses an array to store errors, if they exist, according to the format:

```json
[
    {
        "element": "key name",
        "message": "Error reason"
    }
]
```
Thus, an empty array means a successful validation.

### Schema structure

The schemas must be defined according to the JSON format. For each key, you can define its validation rules using the following options:

|  Key 	| Type  	|  Description 	|
|---	|---	|---	|
| required  	|  Boolean 	|  If its value is `true`, that means the current key is required. For the non-required kyes (optional), if there are other validation rules, they will be performed. 	|
|  pattern 	|  String (Regex) 	|  A valid regex used to evaluate the target value. 	|
|   type	|   String	|  One of the following data types: `string, number, bigint, boolean, object`|
|  minLength 	|  Number 	|  The min length for textual contents and the min value for numbers. 	|
|  maxLength 	|  Number 	|  The max length for textual contents and the max value for numbers. 	|

**For each external key defined within the schema, the algorithm considers it a prerequisite. So, during the evaluation process, if a key is not found, the current object is set as invalid.**

Going over the following schema:

```json
{
    "auth": {
        "auth_time": {
            "required": true,
            "type": "int"
        },
        "exp": {
            "required": true,
            "type": "int" 
        },
        "iat": {
            "required": true,
            "type": "int" 
        },
        "profile": {
            "roles": {
                "required": true,
                "type": "list",
                "minLength": 1,
                "maxLength": 4
            }
        }
    }
}
```
An example of expected input would be:
```json
{
    "auth": {
        "auth_time": 1311280969,
        "exp": 1311281970,
        "iat": 1311280970,
        "sub": "Alice",
        "profile": {
            "email": "user@outlook.com",
            "roles": ["producer", "consumer", "attendant"]
        }
    }
}
```

For this case, the key "`profile`" is required once it is described in the schema definition.

## Computational cost

A JSON document has the same structure as a tree. Each node has a hierarchical tree structure. Thus, iterating through its keys takes linear time, O(n). Complexity is related to the number of levels it has.

For comparison purposes, the JSON provided below:

```
{ 
  node1: 'value', 
  node2: { 
    node3: { 
      node5: 'value'
    }, 
    node4: 'value'
  }
}
```
represents the following tree:

                    root
                  /      \
              node1     node2
                        /   \
                    node3   node4
                    ...

## Running tests

To execute the built-in tests, run the following command:

```bash
py .\main.py
```
**All tests available in the `main.py` file are "handmade," meaning they were written without using any framework specifically designed for testing purposes. It is limited to only a few very basic use cases. Future versions might introduce more sophisticated approaches, such as the [unittest](https://docs.python.org/3/library/unittest.html) framework.**

## Usage examples

To use the **JSON Schema Validator** directly from this project, it is necessary to import the `validate` method located in the "schema_checker.py" file. The `validate` method is simply an interface to the `check` method, which is responsible for performing the JSON validation. So, if you prefer, you can either import the `check` method directly or modify it to suit your requirements.

The `validate` interface requires three parameters: the target dictionary, the corresponding schema, and a callback function used to store the list of potential errors.

A successful validation is achieved when the list of errors is empty.

For instance, let's consider the following schema and data for a hypothetical bill:

*Bill schema*
```json
{
    "productDescription": {
        "required": true,
        "type": "str",
        "minLength": 10,
        "maxLength": 200
    },
    "value": {
        "required": true,
        "type": "float",
        "minValue": 0.01,
        "maxValue": 10e9
    },
    "date": {
        "required": true,
        "type": "str",
        "pattern": "^\\d{4}-\\d{2}-\\d{2}$"
    }
}
```
*Bill data*
```json
{
    "productDescription": "Chopped tomatoes",
    "value": 1.95,
    "date": "2023-06-12"
}
```
It will produce the following output:

![Output for a successfull test](assets/successful-test.png)

On the other hand, if we use the same schema but with the following data:

```json
{
    "value": 1.95,
    "date": "2023-06-12"
}
```
It will produce the following error output:

![Output for an unsuccessfull test](assets/unsuccessful-test.png)

The example below demonstrates a basic usage of the validator.

```python
import json
from schema_checker import validate

def callback (result: list = []):
    print('Success') if len(result) == 0 else print('Faill', result)

data = {
    'productDescription': 'Chopped tomatoes',
    'value': 1.95,
    'date': '2023-06-12'
}
schemaFile = open('schema.json')
# Converts the json file into a dict.
schema = json.load(schema)

validate(data, schema, callback)
```
## Enhancements

Future items to be done:

- Stop recursion when the first inconsistency is found.
  It may be achieved by means of a flag (`proceed` for example), if it is true, the analysts process will continue, on the other hand, it will stop when the first error is reached.

- Validation for arrays: min and max items.

- Built-in validators (ipv4, ipv6, uuid, email, uri, date, etc).