"""
    Default messages.
"""

NOT_FOUND = 'Element not found'
REQUIRED = 'Element required'
TYPE = 'Type mismatch'
MAX_LENGTH = 'Max length surpassed'
PATTERN = 'Unexpected pattern'
